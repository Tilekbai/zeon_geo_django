from django.views import generic
from django.contrib.gis.geos import fromstr
from django.contrib.gis.db.models.functions import Distance
from .models import Shop


longitude = -47.52
latitude = 74.58

user_location = fromstr(
                        f'POINT({longitude} {latitude})', srid=4326
                    )

class ShopList(generic.ListView):
    model = Shop
    context_object_name = 'shops'
    queryset = Shop.objects.annotate(distance=Distance('location',
    user_location)
    ).order_by('distance')[0:6]
    template_name = 'shop_list.html'
